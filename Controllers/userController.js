const mongoose = require("mongoose");
const User = require("../Models/usersSchema.js");
const Course = require("../Models/coursesSchema.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// Controllers

//This controller will create or register a user on our db/database
module.exports.userRegistration = (request, response) => {

		const input = request.body;

		User.findOne({email: input.email})
		.then(result => {
			if(result !== null){
				return response.send("The email is already taken!")
			}else{
				let newUser = new User({
					firstName: input.firstName,
					lastName: input.lastName,
					email: input.email,
					password: bcrypt.hashSync(input.password, 10),
					mobileNo: input.mobileNo
				})

				//save to database
				newUser.save()
				.then(save => {
					return response.send("You are now registered to our website!")
							})
				.catch(error => {
					return response.send(error)
				})
			}
		})

		.catch(error => {
			return response.send(error)
		})
}

//User Authentication 	
module.exports.userAuthentication = (request, response) => {
		let input = request.body;

		//Possible scenarios in logging in
			//1. email is not yet registerd.
			//2. email is registered but the password is wrong

		User.findOne({email: input.email})
		.then(result => {
			if(result === null){
				return response.send("Email is not yet registered. Register first before logging in!")
			}else{
				//we have to verify if the password is correct
				//The "compareSync" method is used to compare a non encrypted password to the encrypted password.
				//it returns boolean value, if match true value will return otherwise false.
				const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

				if(isPasswordCorrect){
					return response.send({auth: auth.createAccessToken(result)});
				}else{
					return response.send("Password is incorrect!")
				}
			}

		})

		.catch(error => {
			return response.send(error)
		})
}

//Retrieve User Details
/*module.exports.getProfile = (request, response) => {
		let input = request.body;

		User.findOne({_id: input._id})
		.then(result => {
			if(result === null){
				return response.send("User is not yet registered!")
		}
		result.password = "";
			return response.send(result);
		})
		.catch(error => {
			return response.send(error);
		})
}
*/
module.exports.getProfile = (request, response) => {
	// let input = request.body;
		const userData = auth.decode(request.headers.authorization);

		console.log(userData);
		
		return User.findById(userData._id).then(result => {
			//avoid to expose sensitive information such as password.
			result.password = "";

			return response.send(result);
		})
	}


// Controller for user enrollment:
	// 1. We can get the id of the user by getting/decoding the jwt
	// 2. We can get the courseId by using the request params.

module.exports.enrollCourse = async (request, response) => {
	// First we have to get the userId and the course Id

	//decode the token to extract/unpack the payload
	const userData = auth.decode(request.headers.authorization);
	
	//get the courseId by targetting the params in the url
	const courseId = request.params.courseId;

	//2 things that we need to do in this controller
		//First, to push the courseId in the enrollments property of the user
		//Second, to push the userId in the enroller's property of the course.


if(userData.isAdmin){
		return response.send("You don't have access in this page!");
	}else {
		let isUserUpdated = await User.findById(userData._id)
			.then(result => {
				if(result === null){
					return false;
				}else{
					result.enrollments.push({courseId: courseId});

					return result.save()
					.then(save => true)
					.catch(error => false)
				}
			})

			Course.findById(courseId)
			.then(result => {
				if(result === null){
					return response.send("CourseId is invalid, please try again!")
				}else{
					result.enrolles.push({userId: userData._id});
					return response.send("The course is now enrolled!")
					return result.save()
					.then(save => true)
					.catch(error => false)
				}

			})
		}
	}