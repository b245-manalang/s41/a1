const Course = require("../Models/coursesSchema.js");
const auth = require("../auth.js");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/

module.exports.addCourse = (request, response) =>{
	// 
	const userAdmin = auth.decode(request.headers.authorization);

		if(userAdmin.isAdmin === true){
			let input = request.body;

			let newCourse = new Course({
				name: input.name,
				description: input.description,
				price: input.price
			});
		
			//saves the created object to our database
			return newCourse.save()
			.then(course =>{
				console.log(course);
				response.send(true);
			})
			// course creation failed
			.catch(error => {
				console.log(error);
				response.send(false);
			})
		}else{
			return response.send("User is not an admin!");
		}
	}

// Create a controller wherein it will retrieved all the courses (activeonactive Courses)

module.exports.allCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);

	if(!userData.isAdmin){
		return response.send("You don't have access to this route!");
	}else{
		Course.find({})
		.then(result=> response.send(result))
		.catch(error => response.send(error));
	}
}

// Create a controller wherein it will retrieve course that are active.
module.exports.allActiveCourses = (request, response) => {

	Course.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error));
}



/*
	Mini Activity:
		1. You are going to create a route wherein it can retrieve all inactive courses.
		2. Make sure that the admin users only are the ones that can access this route.
*/

module.exports.allInactiveCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);

	if(!userData.isAdmin){
		return response.send("You don't have access to this route!");
	}else{
		Course.find({isActive: false})
		.then(result=> response.send(result))
		.catch(error => response.send(error));
	}
}
//This controller will get the details of specific course
module.exports.courseDetails = (request, response) => {
	//to get the params from the url
	const courseId = request.params.courseId;

	Course.findById(courseId)
		.then(result => response.send(result))
		.catch(error => response.send(error));
}

//This controller is for updating specific course
/*
	Business logic:
		1. We are going to edit/update the course that is stored in the params
*/

module.exports.updateCourse = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;

	const input = request.body

	if(!userData.isAdmin){
		return response.send("You don't have access in this page!");
	}else{
		Course.findOne({_id: courseId})
		.then(result =>{
			if (result === null){
				return response.send("CourseId is invalid, please try again!")
		}else{
		let updatedCourse = {
			name: input.name,
			description: input.description,
			price: input.price
		}

		Course.findByIdAndUpdate(courseId, updatedCourse, {new: true})
		.then(result => {
			console.log(result);
			return response.send(result)})
		.catch(error => response.send(error));
		}
	})
	}
}

module.exports.archiveCourse = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;

	const input = request.body

	if(!userData.isAdmin){
		return response.send("You don't have access in this page!");
	}else{
		Course.findOne({_id: courseId})
		.then(result =>{
			if (result === null){
				return response.send("CourseId is invalid, please try again!")
		}else{
		let archivedCourse = {
			isActive: false
		}

		Course.findByIdAndUpdate(courseId, archivedCourse, {new: true})
		.then(result => {
			console.log(result);
			return response.send(true)})
		.catch(error => response.send(error));
		}
	})
	}
}
